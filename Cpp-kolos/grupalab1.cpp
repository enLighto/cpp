#include <iostream>
#include <fstream>
#include <string>

using namespace std;

struct student
{
  string imie;
  string nazwisko;
  int nr_indeksu;
  double ocena_t;
  double ocena_p;

};

struct egzamin
{
   string nazwa;
   int ile;
   student *wyniki;
};

egzamin wczytaj(istream &plik)
{
    egzamin Eegzamin;
    if(plik.good())
    {
        plik>>Eegzamin.nazwa;
        plik>>Eegzamin.ile;
        Eegzamin.wyniki = new student[Eegzamin.ile];
        for(int i = 0; i<Eegzamin.ile; i++)
        {
            plik>>Eegzamin.wyniki[i].imie;
            plik>>Eegzamin.wyniki[i].nazwisko;
            plik>>Eegzamin.wyniki[i].nr_indeksu;
            plik>>Eegzamin.wyniki[i].ocena_t;
            plik>>Eegzamin.wyniki[i].ocena_p;
        }
    }
    return Eegzamin;
}

void wyswietl (const egzamin *Eegzamin)
{
    for (int i=0; i<Eegzamin->ile;i++)
    {
        cout <<"Imie : " << Eegzamin->wyniki[i].imie << endl;
        cout <<"Nazwisko : " << Eegzamin->wyniki[i].nazwisko << endl;
        cout <<"nr indeksu : " << Eegzamin->wyniki[i].nr_indeksu << endl;
        cout <<"ocena_t : " << Eegzamin->wyniki[i].ocena_t << endl;
        cout <<"ocena_p : " << Eegzamin->wyniki[i].ocena_p << endl;
        float srednia;
        srednia =(0.2*(Eegzamin->wyniki[i].ocena_t)) +(0.8*(Eegzamin->wyniki[i].ocena_p));
        cout << "srednia wazona: " << srednia <<endl;

    }
}

int main(int argc, char *argv[])
{
    ifstream plik(argv[1]);
    if(!plik.good())
    {
        cout <<"plik nie dziala" << endl;
    }
    egzamin Eegzamin = wczytaj(plik);
    wyswietl(&Eegzamin);
    delete[] Eegzamin.wyniki;

    return 0;

}