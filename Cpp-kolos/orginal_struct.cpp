#include <iostream>
#include <fstream>
#include <string>

using namespace std;

struct SStudent
{
    string imie;
    string nazwisko;
    unsigned int iloscNiezaliczonych;
    string *niezaliczone;
};

struct SWykaz
{
    unsigned int iloscStudentow;
    SStudent *lista;
};

SWykaz wczytaj(istream &input)
{   
    SWykaz wykaz;
    if (input.good())
    {
        input >> wykaz.iloscStudentow;
        if (wykaz.iloscStudentow > 0)
        {
            wykaz.lista = new SStudent[wykaz.iloscStudentow];
        }
        
        for (unsigned int i = 0; i < wykaz.iloscStudentow; i++)
        {
            input >> wykaz.lista[i].imie;
            input >> wykaz.lista[i].nazwisko;
            input >> wykaz.lista[i].iloscNiezaliczonych;
            
            if (wykaz.lista[i].iloscNiezaliczonych > 0)
            {
                wykaz.lista[i].niezaliczone = new string[wykaz.lista[i].iloscNiezaliczonych];  
                
                for (unsigned int j = 0; j < wykaz.lista[i].iloscNiezaliczonych; j++)
                {
                    input >> wykaz.lista[i].niezaliczone[j];
                }
            }
        }
    }
    else
        return wykaz;
    
    return wykaz;    
}

void wyswietl(const SWykaz *wykaz)
{
     for (unsigned int i = 0; i < wykaz->iloscStudentow; i++)
     {
         cout << "Imie i nazwisko: " << wykaz->lista[i].imie << " " << wykaz->lista[i].nazwisko << endl;
         cout << "Ilosc niezaliczonych kursow: " << wykaz->lista[i].iloscNiezaliczonych << endl;
         cout << "Lista niezaliczonych kursow: " << endl;
         for (unsigned int j = 0; j < wykaz->lista[i].iloscNiezaliczonych; j++)
         {
             cout << "-> " << wykaz->lista[i].niezaliczone[j] << endl;
         }
         cout << endl;
     }
}

void wyswietl(const SWykaz &wykaz)
{
     for (unsigned int i = 0; i < wykaz.iloscStudentow; i++)
     {
         cout << "Imie i nazwisko: " << wykaz.lista[i].imie << " " << wykaz.lista[i].nazwisko << endl;
         cout << "Ilosc niezaliczonych kursow: " << wykaz.lista[i].iloscNiezaliczonych << endl;
         cout << "Lista niezaliczonych kursow: " << endl;
         for (unsigned int j = 0; j < wykaz.lista[i].iloscNiezaliczonych; j++)
         {
             cout << "-> " << wykaz.lista[i].niezaliczone[j] << endl;
         }
         cout << endl;
     }
}

void usun(SWykaz *wykaz)
{   
    for (unsigned int i = 0; i < wykaz->iloscStudentow; i++)
    {
        delete []wykaz->lista[i].niezaliczone;
    }
    delete []wykaz->lista;
}

void usun(SWykaz &wykaz)
{
    for (unsigned int i = 0; i < wykaz.iloscStudentow; i++)
    {
        delete []wykaz.lista[i].niezaliczone;
    }
    delete []wykaz.lista;
}

int main(int argc, char *argv[])
{   
    // Sprawdzamy ilosc argumentow, jesli nie podamy pliku z danymi wejsciowymi
    // aplikacja wyswieli komunikat
    if (argc < 2)
    {
        cerr << "Za malo parametrow!" << endl ;
        return -1;           
    }
    
    // Otwieramy plik przekazany jako argument wywolania programu
	ifstream plik(argv[1]);
    if(!plik)
    {
        cerr << "blad otwarcia pliku!" << endl ;
        return -1;
    }
    else
    {
        SWykaz lista = wczytaj(plik);
        
        // Wywolanie funkcji z *
        //wyswietl(&lista);
        //usun(&lista);
        
        // Wywolanie funkcji z &
        wyswietl(lista);
        usun(lista);
    }
    plik.close();
    return 0;
}