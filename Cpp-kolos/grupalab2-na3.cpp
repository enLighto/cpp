#include <iostream>
#include <fstream>
#include <string>

using namespace std;

struct jaskinia{
    string nazwa;
    double dlugos;
    double glebokosc;
    string polozenie;
};

struct wykaz{
    int rozmiar;
    jaskinia *lista;
};

wykaz wczytaj(istream &plik)
{
    wykaz Swykaz;
    if(plik.good())
    {
        plik >> Swykaz.rozmiar;
        Swykaz.lista = new jaskinia[Swykaz.rozmiar];
    }
    for(int i = 0; i < Swykaz.rozmiar ;i++) {
        plik >> Swykaz.lista[i].nazwa;
        plik >> Swykaz.lista[i].dlugos;
        plik >> Swykaz.lista[i].glebokosc;
        plik >> Swykaz.lista[i].polozenie;

    }
    return Swykaz;
}

void wyswietl(const wykaz *Swykaz)
        {
            for (int i =0; i < Swykaz->rozmiar;i++)
            {
                cout << "Nazwa: " << Swykaz->lista[i].nazwa << endl;
                cout << "Dlugosc: " << Swykaz->lista[i].dlugos << endl;
                cout << "Glebokosc: " << Swykaz->lista[i].glebokosc << endl;
                cout << "polozenie: " << Swykaz->lista[i].polozenie << endl;

            }
        }
int main(int argc, char *argv[])
{
    ifstream plik(argv[1]);
    if(!plik.good())
    {
        cout << "Error" << endl;
    }

    wykaz Swykaz = wczytaj(plik);
    wyswietl(&Swykaz);
    delete [] Swykaz.lista;

    return 0;

}