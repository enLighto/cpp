#include <iostream>
#include <fstream>
using namespace std;

struct SStudent{
    string imie;
    string nazwisko;
    unsigned int ilosc;
    string *niezaliczone;
    SStudent()
    {
        imie = "";
        nazwisko = "";
        ilosc = 0;
        niezaliczone = 0;
    }

    ~SStudent(){
        niezaliczone = 0;
    }
};

struct SWykaz
{
    unsigned int ilosc;
    SStudent *lista;
    SWykaz()
    {
        ilosc = 0;
        lista = 0;
    }
    ~SWykaz()
    {
        lista = 0;
    }
};

void usun(SWykaz *student)
{
    for (unsigned int i=0; i < student -> ilosc; i++)
    {
        delete []student->lista[i].niezaliczone;
        student->lista[i].niezaliczone = 0;
    }
    delete []student->lista;
    student->lista=0;
}

void usun_generowana(SWykaz *student, unsigned int rozmiar)
{
    if (rozmiar > 0)
        for (unsigned int i = 0; i < rozmiar; i++)
        {
            delete []student->lista[i].niezaliczone;
            student->lista[i].niezaliczone=0;
        }
    delete []student->lista;
        student->lista=0;
}
SWykaz wczytaj(istream &input)
{
    SWykaz student;
    unsigned int ile_rek = 0;
    bool ok = true, tablica1 = false;
    if (input.good()) ok = false; else
    {
        input >> student.ilosc;
        if (student.ilosc > 0)
        {
            student.lista = new SStudent[student.ilosc];
            tablica1 = true;
        }
    }
    for (unsigned int i = 0; i < student.ilosc; i++)
    {
        if (!input.good()) ok = false; else
            input >> student.lista[i].imie;
        if (!input.good()) ok = false; else
            input >> student.lista[i].nazwisko;
        if (!input.good()) ok = false; else
            {
            input >> student.lista[i].ilosc;
            if (student.lista[i].ilosc > 0)
            {
                student.lista[i].niezaliczone = new string[student.lista[i].ilosc];
                ile_rek++;
            }

            for (unsigned int j = 0; j < student.ilosc; j++)
            {
                if (!input.good()) ok = false; else
                    input >> student.lista[i].niezaliczone[j];
            }
        }
    }
        return student;
}

void wyswietl(const SWykaz *student)
{
    for (unsigned int i =0; i < student -> ilosc; i++)
    {
        cout << "Imie i nazwisko: " << student->lista[i].imie << " " << student->lista[i].nazwisko << endl;
        cout << "Ilosc niezaliczonych kursow: " << student->lista[i].ilosc << endl;
        cout << "Lista niezaliczonych kursow: " << endl;
        for (unsigned int j = 0; j < student->lista[i].ilosc; j++)
        {
            cout << "->" << student->lista[i].niezaliczone[j] << endl;
        }
        cout << endl;
    }
}

int main(int argc, char *argv[])
{
    if (argc < 2)
    {
        cerr << "za malo param" << endl;
        return -1;
    }
    ifstream plik(argv[1]);
    if(!plik)
    {
        cerr << "blad otwarcia pliku!" << endl;
        return -1;
    }
    else
    {
        SWykaz student = wczytaj(plik);
        wyswietl(&student);
        usun(&student);
    }
    plik.close();
    return 0;
}

