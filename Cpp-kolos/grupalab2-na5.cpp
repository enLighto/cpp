#include <iostream>
#include <fstream>
#include <string>
 
using namespace std;
 
struct Jaskinia {
    string nazwa, polozenie;
    double dlugosc, glebokosc;
};
 
struct Wykaz {
    int rozmiar;
    Jaskinia* jaskinie;
};
 
void wczytaj(Wykaz &wykaz, ifstream& ifs) {
    int rozmiar;
 
    string nazwa, polozenie;
    double dlugosc, glebokosc;
 
    ifs >> rozmiar;
    wykaz.rozmiar = rozmiar;
    wykaz.jaskinie = new Jaskinia[rozmiar];
 
    for(int i = 0; i < rozmiar; i++){
        Jaskinia jaskinia;
 
        ifs >> nazwa >> dlugosc >> glebokosc >> polozenie;
 
        jaskinia.nazwa = nazwa;
        jaskinia.dlugosc = dlugosc;
        jaskinia.glebokosc = glebokosc;
        jaskinia.polozenie = polozenie;
 
        wykaz.jaskinie[i] = jaskinia;
    }
}
 
void wyswietl(Wykaz &wykaz) {
    Jaskinia jaskinia;
    for(int i = 0; i < wykaz.rozmiar; i++){
        jaskinia = wykaz.jaskinie[i];
 
        cout << "Nazwa: " << jaskinia.nazwa << endl;
        cout << "Dlugosc: " << jaskinia.dlugosc << endl;
        cout << "Glebokosc: " << jaskinia.glebokosc << endl;
        cout << "Polozenie: " << jaskinia.polozenie << endl;
        cout << endl;
    }
}
 
void min_max(Wykaz &wykaz, Jaskinia&max, Jaskinia &min){
    Jaskinia jaskinia;
 
    max = wykaz.jaskinie[0];
    min = wykaz.jaskinie[0];
 
    for(int i = 0; i < wykaz.rozmiar; i++){
        jaskinia = wykaz.jaskinie[i];
 
        if(jaskinia.dlugosc > max.dlugosc)
            max = jaskinia;
 
        if(jaskinia.dlugosc < min.dlugosc)
            min = jaskinia;
    }
}
 
void szukaj(Wykaz &wykaz, string nazwa, ofstream &ofs) {
     Jaskinia jaskinia;
 
    for(int i = 0; i < wykaz.rozmiar; i++){
         jaskinia = wykaz.jaskinie[i];
 
        if(jaskinia.nazwa == nazwa){
            ofs << "Nazwa: " << jaskinia.nazwa << endl;
            ofs << "Dlugosc: " << jaskinia.dlugosc << endl;
            ofs << "Glebokosc: " << jaskinia.glebokosc << endl;
            ofs << "Polozenie: " << jaskinia.polozenie << endl;
            ofs << endl;
 
            return;
        }
    }
}
 
void usun(Wykaz &wykaz){
    delete[] wykaz.jaskinie;
}
 
int main(int argc, char* argv[]) {
    ifstream ifs(argv[1]);
    ofstream ofs("jaskinia.txt");
 
 
    if(ifs.good() && ofs.good() && argv[2]){
        string szukana_nazwa = argv[2];
        Wykaz wykaz;
        Jaskinia max, min;
        
 
        wczytaj(wykaz, ifs);
        wyswietl(wykaz);
 
        min_max(wykaz, max, min);
 
        cout << "Jaskinia o najwiekszej dlugosci: " << max.nazwa << endl;
        cout << "Jaskinia o najmniejszej dlugosci: " << min.nazwa << endl;
 
        szukaj(wykaz, szukana_nazwa, ofs);
        
        usun(wykaz);
    }
 
    return 0;
}