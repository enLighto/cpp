#include <iostream>

using namespace std;

int* WypelnijTablice(int* Tablica, int n)
{
    int* TablicaWewnetrzna = new int[n];

    for (int i=0; i<n; i++) {
        if (i%2 == 0) {
            TablicaWewnetrzna[i] = i*2;
        } else {
            TablicaWewnetrzna[i] = i-1;
        }
    };

    return TablicaWewnetrzna;
};

int main()
{
    int n;

    cout << "Podaj liczbe n: ";
    cin >> n;

    int* TablicaZewnetrzna = WypelnijTablice(TablicaZewnetrzna, n);

    cout << endl;

    for (int i=0; i<n; i++) {
        cout << TablicaZewnetrzna[i] << endl;
    };
    delete[] TablicaZewnetrzna;
    return 0;
}