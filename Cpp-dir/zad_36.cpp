#include <iostream>
#include <math.h>

using namespace std;

float pole(float a, float b, float c) {
    float p=(a+b+c)/2;
    float P=sqrt(p*(p-a)*(p-b)*(p-c));
    return P;
}

int main()
{
    cout << pole(10, 10, 10) << endl;
    return 0;
}