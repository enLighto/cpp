#include <iostream>

using namespace std;

int MaksymalnaWartosc(int* Tablica, int n)
{
    int x = Tablica[0];

    for (int i=0; i<n; i++) {
      if (Tablica[i] > x) {
        x = Tablica[i];
      };
    };

    return x;
};

int main()
{
    int PrzykladowaTablica[] = {1, 233, -22, -3, 2330};

    cout << "Maksymalna wartosc w tablicy to: " << MaksymalnaWartosc(PrzykladowaTablica, 5);

    return 0;
}