#include <iostream>
#include <stdlib.h>
#include <math.h>

using namespace std;

int x;
float a, b, n;

void wprowadzab() {
    cout << "Wprowadz a: ";
    cin >> a;
    cout << "Wprowadz b: ";
    cin >> b;
}

void wprowadzan() {
    cout << "Wprowadz a: ";
    cin >> a;
    cout << "Wprowadz n: ";
    cin >> n;
}

float suma(float a, float b) { return a+b; }
float roznica(float a, float b) { return a-b; }
float iloczyn(float a, float b) { return a*b; }
float iloraz(float a, float b) { return a/b; }
float piergwiastek(float a, float n) { return pow(a, 1/n); }
float potega(float a, float n) { return pow(a, n); }

bool kalkulator() {
    cout << "[1] Dodawanie" << endl;
    cout << "[2] Odejmowanie" << endl;
    cout << "[3] Mnozenie" << endl;
    cout << "[4] Dzielenie" << endl;
    cout << "[5] Piergwiastkowanie" << endl;
    cout << "[6] Potegowanie" << endl;

    cout << endl << "[0] Zakoncz" << endl;

    cout << endl << "Wybor: ";
    cin >> x;

    system("cls");

    switch(x) {
    case 1:
        wprowadzab();
        system("cls");
        cout << a << "+" << b << " = " << suma(a,b) << endl << endl;
        break;

    case 2:
        wprowadzab();
        system("cls");
        cout << a << "-" << b << " = " << roznica(a,b) << endl << endl;
        break;

    case 3:
        wprowadzab();
        system("cls");
        cout << a << "*" << b << " = " << iloczyn(a,b) << endl << endl;
        break;

    case 4:
        wprowadzab();
        system("cls");
        cout << a << "/" << b << " = " << iloraz(a,b) << endl << endl;
        break;

    case 5:
        wprowadzan();
        system("cls");
        cout << "sqrt(" << a << ", " << n << ") = " << piergwiastek(a, n) << endl << endl;
        break;

    case 6:
        wprowadzan();
        system("cls");
        cout<< "pow(" << a << ", " << n << ") = " << potega(a, n) << endl << endl;
        break;

    case 0:
        exit(1);
    }
}

int main()
{
    while(kalkulator());

    return 0;
}