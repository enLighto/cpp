#include <iostream>

using namespace std;

int* polowka(int* tablica, int rozmiar) {
	for (int i = 0; i < (rozmiar/2); i++) {
		tablica[i] = 0;
	};

	for (int j = rozmiar; j >= (rozmiar/2); j--) {
		tablica[j] = j+1;
	};

	return 0;
}

int main() {
	int tablica1[7] = {1, 2, 3, 4, 5, 6, 7};

	polowka(tablica1, 7);

	for (int i = 0; i < 7; i++) {
		cout << tablica1[i] << endl;
	};

	return 0;
}