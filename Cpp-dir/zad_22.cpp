#include <iostream>

using namespace std;

int** tabliczka_mnozenia(int rozmiar) {
	int** tablica_wewnetrzna = new int*[rozmiar]; 
	for (int x=0; x < rozmiar; x++) {
		tablica_wewnetrzna[x] = new int[rozmiar];
	};

	for (int i = 0; i < rozmiar; i++) {
		for (int j = 0; j < rozmiar; j++) {
			tablica_wewnetrzna[i][j] = i*j;
		};
	};

	return tablica_wewnetrzna;
}

int main() {
	int rozmiar = 5;
	int** tablica1;

	tablica1 = tabliczka_mnozenia(rozmiar);

	for (int i = 0; i < rozmiar; i++) {
		for (int j = 0; j < rozmiar; j++) {
			cout << tablica1[i][j] << " ";
		};

		cout << endl;
	};

	return 0;
}