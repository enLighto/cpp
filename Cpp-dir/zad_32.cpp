#include <iostream>
#include <math.h>

using namespace std;

int kwadrat(int a) {
    return pow(a, 2);
}

int main()
{
    cout << kwadrat(4) << endl;
    return 0;
}