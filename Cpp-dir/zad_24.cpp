#include <iostream>

using namespace std;

int* wytnij(int* tablica, int rozmiar, int od, int az_do) {
    int* tablica_wewnetrzna = new int[az_do-od];

    int j=0;
    for (int i=od; i<=az_do; i++) {
        tablica_wewnetrzna[j] = tablica[i];
        j++;
    };

    return tablica_wewnetrzna;
};

int main()
{
    int tablica1[4] = {1, 2, 3, 4};

    int* tablica2 = wytnij(tablica1, 4, 2, 3);

    for (int i=0; i<2; i++) {
        cout << tablica2[i] << ' ';
    }
    return 0;
}