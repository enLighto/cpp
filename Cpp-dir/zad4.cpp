#include <iostream>
#include <cstdlib>

using namespace std;
 
unsigned long fib_r(const unsigned int n)
{
    if(n == 0) return 0;
    if(n == 1) return 1;
    return fib_r(n-1)+fib_r(n-2);
}
 
int main()
{
    int n;
 
    cout << "Podaj numer wyrazu ciagu Fibonacciego do obliczenia:" << endl;
    cin >> n;
 
    cout << fib_r(n) << endl;
	return 0;
}