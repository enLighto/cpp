#include <iostream>
#include <cmath>

using namespace std;

int* WypelnijTablice(int* Tablica, int n)
{
    int* TablicaWewnetrzna = new int[n];

    for (int i=0; i<n; i++) {
        if (i%2 == 0) {
            TablicaWewnetrzna[i] = 0;
        } else {
            TablicaWewnetrzna[i] = 2;
        };
    };

    return TablicaWewnetrzna;
};

int main()
{
    int n;

    cout << "Podaj liczbe n: ";
    cin >> n;

    int* TablicaZewnetrzna = WypelnijTablice(TablicaZewnetrzna, n);

    cout << endl;

    for (int i=0; i<n; i++) {
        cout << TablicaZewnetrzna[i] << endl;
    };
    return 0;
}