#include <iostream>

using namespace std;

int* odwroc(int* tablica, int rozmiar) {
    int* tablica_wewnetrzna = new int[rozmiar];

    int j=0;
    for (int i=rozmiar-1; i>=0; i--) {
        tablica_wewnetrzna[j] = tablica[i];
        j++;
    }

    return tablica_wewnetrzna;
}

int main()
{
    int tablica1[10] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
    int* tablica2 = odwroc(tablica1, 10);

    for (int i=0; i<10; i++) {
        cout << tablica2[i] << ' ';
    }
    return 0;
}