#include <iostream>

using namespace std;

int* dodaj(int* tablica, int rozmiar) {
	int* tablica_wewnetrzna = new int[rozmiar];

	for (int i = 0; i < rozmiar; i++) {
		tablica_wewnetrzna[i] = tablica[i] + 1;
	};

	return tablica_wewnetrzna;
}

int main() {
	int* tablica1 = new int[4];
	tablica1[0] = 1;
	tablica1[1] = 2;
	tablica1[2] = 3;
	tablica1[3] = 4;

	tablica1 = dodaj(tablica1, 4);

	for (int i = 0; i < 4; i++) {
		cout << tablica1[i] << endl;
	};
}