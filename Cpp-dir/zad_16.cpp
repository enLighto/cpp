#include <iostream>

using namespace std;

int SredniaArtmetycznaTablicy(int* Tablica, int n)
{
    int x = 0;

    for (int i=0; i<n; i++) {
        x = x + Tablica[i];
    };

    return x/n;
};

int main()
{
    int PrzykladowaTablica[] = {1, 2, 3, 4, 5};

    cout << "Srednia artmetyczna tablicy wynosci: " << SredniaArtmetycznaTablicy(PrzykladowaTablica, 5);

    return 0;
}