#include <iostream>

using namespace std;

int IndexMaksymalnaWartosc(int* Tablica, int n)
{
    int x = Tablica[0];

    for (int i=0; i<n; i++) {
      if (Tablica[i] > x) {
        x = i;
      };
    };

    return x;
};

int main()
{
    int PrzykladowaTablica[] = {1, 233, -22, -3, 2330};

    cout << "Indeks maksymalnej wartosci w tablicy to: " << IndexMaksymalnaWartosc(PrzykladowaTablica, 5);

    return 0;
}