#include <iostream>

using namespace std;

int* kasuj_element(int* tablica, int rozmiar, int index) {
	int nowy_rozmiar = rozmiar - 1;
	int* tablica_wewnetrzna = new int [nowy_rozmiar];

	int j = 0;

	for (int i = 0; i <  rozmiar; i++) {
		if (i != index) {
			tablica_wewnetrzna[j] = tablica[i];
			j++;
			//cout << tablica_wewnetrzna[i] << endl;
		};
	};

	return tablica_wewnetrzna;
}

int main() {
	int* tablica1 = new int [5];
	tablica1[0] = 1;
	tablica1[1] = 2;
	tablica1[2] = 3;
	tablica1[3] = 4;
	tablica1[4] = 5;

	int* tablica2 = kasuj_element(tablica1, 5, 3);

	for (int i = 0; i < 4; i++) {
		cout << tablica2[i] << endl;
	};

	return 0;
}