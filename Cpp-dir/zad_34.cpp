#include <iostream>
#include <math.h>

using namespace std;

float suma(float a, float b) { return a+b; }
float roznica(float a, float b) { return a-b; }
float iloczyn(float a, float b) { return a*b; }
float iloraz(float a, float b) { return a/b; }
float kwadrat(float a) { return pow(a, 2); }
float szescian(float a) { return pow(a, 3); }

int main()
{
    float x = 2;
    cout << iloczyn(suma(iloczyn(10, szescian(x)), iloczyn(3.14, kwadrat(x))), roznica(iloraz(x, 3), iloraz(1, kwadrat(x)))) << endl;
    return 0;
}