#include <iostream>

using namespace std;

int tablica1[5] = {1, 2, 3, 4, 5};
int* tablica2 = new int;

void skopjuj_do(int* tablica, int rozmiar, int* tablica_docelowa) {
    for (int i=0; i<rozmiar; i++) {
       tablica_docelowa[i] = tablica[i];
    }
}

int main()
{
    skopjuj_do(tablica1, 5, tablica2);

    for (int i=0; i<5; i++) {
        cout << tablica2[i] << " ";
    }
    return 0;
}