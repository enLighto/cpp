#include <iostream>
#include <cstdlib>
 
using namespace std;
 
unsigned long fib_i(const unsigned int n)
{
    int a, b;
    if(n == 0) return 0;
 
    a = 0; b = 1;
    for(int i=0; i < (n-1); i++)
    {
        swap(a, b);
        b += a;
    }
    return b;
}
 
int main()
{
    int n;
    cout << "Podaj wyraz ciagu fibonacciego do obliczenia:" << endl;
    cin >> n;
    cout << fib_i(n) << endl;
    return(0);
}