#include <iostream>

using namespace std;

int* statystyki(int* tablica, int rozmiar, int maksymalna) {
	int* tablica_wewnetrzna = new int[maksymalna];

	for (int i = 0; i < rozmiar; i++) {
		int suma = 0;

		for (int j = 0; j < rozmiar; j++) {
			if (tablica[j] == i) {
				suma++;
			}
		};

		tablica_wewnetrzna[i] = suma;
	};

	return tablica_wewnetrzna;
}

int main() {
	int* tablica1 = new int[4];

	tablica1[0] = 1;
	tablica1[1] = 1;
	tablica1[2] = 0;
	tablica1[3] = 2;

	tablica1 = statystyki(tablica1, 4, 2);

	for (int i = 0; i < 4; i++) {
		cout << i << ": " << tablica1[i] << endl;
	};

	return 0;
}